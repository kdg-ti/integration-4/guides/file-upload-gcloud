const formElement = document.getElementById("upload-form")
const uploadButton = document.getElementById("upload-button")
const imageElement = document.getElementById("uploaded-image")

async function uploadFile() {
    const resultElement = formElement.elements.namedItem("result")
    const formData = new FormData(formElement)

    try {
        const response = await fetch("/api/files", {
            method: "POST",
            body: formData
        })

        resultElement.value = `Result status code: ${response.status}.`

        if (response.status === 200) {
            const responseObject = await response.json()
            imageElement.src = responseObject.url
            console.log(`Here is the image in the cloud: ${responseObject.url}`)
        }
    } catch (error) {
        console.error("Error: ", error)
    }
}

uploadButton.addEventListener("click", uploadFile)
