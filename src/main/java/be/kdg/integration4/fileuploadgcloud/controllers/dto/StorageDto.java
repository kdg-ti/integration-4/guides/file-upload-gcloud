package be.kdg.integration4.fileuploadgcloud.controllers.dto;

public record StorageDto(String url) {
}
