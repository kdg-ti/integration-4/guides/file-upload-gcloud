package be.kdg.integration4.fileuploadgcloud.controllers;

import be.kdg.integration4.fileuploadgcloud.controllers.dto.StorageDto;
import be.kdg.integration4.fileuploadgcloud.services.CloudStorageRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/files")
public class FilesController {
    private final CloudStorageRepository cloudStorageRepository;

    public FilesController(CloudStorageRepository cloudStorageRepository) {
        this.cloudStorageRepository = cloudStorageRepository;
    }

    @PostMapping
    public ResponseEntity<StorageDto> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        var mediaLink = cloudStorageRepository.uploadToBucket(file.getInputStream());
        return ResponseEntity.ok().body(new StorageDto(mediaLink));
    }
}
