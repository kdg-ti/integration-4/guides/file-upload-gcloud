package be.kdg.integration4.fileuploadgcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileUploadGcloudJavaApplication {
    public static void main(String[] args) {
        SpringApplication.run(FileUploadGcloudJavaApplication.class, args);
    }
}
