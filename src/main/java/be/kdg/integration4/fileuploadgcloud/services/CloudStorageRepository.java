package be.kdg.integration4.fileuploadgcloud.services;

import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

@Repository
@PropertySource("classpath:storage.properties")
public class CloudStorageRepository {
    private static final Logger LOGGER = Logger.getLogger(CloudStorageRepository.class.getName());

    private final String projectId;
    private final String bucketName;

    public CloudStorageRepository(@Value("${demo.project-id}") String projectId,
                                  @Value("${demo.bucket-name}") String bucketName) {
        this.projectId = projectId;
        this.bucketName = bucketName;
    }

    /*
     * https://cloud.google.com/storage/docs/samples/storage-upload-file
     */
    public String uploadToBucket(InputStream inputStream) throws IOException {
        var objectName = "a_very_hardcoded_object_name";
        var storage = StorageOptions.newBuilder().setProjectId(projectId).build().getService();
        var blobId = BlobId.of(bucketName, objectName);
        var blobInfo = BlobInfo
                .newBuilder(blobId)
                .setContentType(MediaType.IMAGE_JPEG_VALUE)
                .build();

        Storage.BlobWriteOption precondition;
        if (storage.get(bucketName, objectName) == null) {
            precondition = Storage.BlobWriteOption.doesNotExist();
        } else {
            precondition = Storage.BlobWriteOption.generationMatch(storage.get(bucketName, objectName).getGeneration());
        }
        var blob = storage.createFrom(blobInfo, inputStream, precondition);

        LOGGER.info("File uploaded to bucket " + bucketName + " with media link " + blob.getMediaLink());

        return blob.getMediaLink();
    }
}
