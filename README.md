# File Upload GCloud

## Instructions

**This won't work on your machine because you don't have write access to my bucket!**

Run the following command from the project root to build the project.
```shell
./gradlew build
```

To start the project in http mode on port 8080, run the following command from the project root.
```shell
./gradlew bootRun
```

Next, navigate to [http://localhost:8080](http://localhost:8080) and upload your file.  
Currently, the MIME type is hard-coded to be a JPG image only.

## Documentation

The file uploading feature is based on [Baeldung's example](https://www.baeldung.com/spring-file-upload).

The cloud bucket uploading feature is based on [Google's example](https://cloud.google.com/storage/docs/samples/storage-upload-file).

## Important

When testing the bucket from `localhost`, you'll need to provide some __credentials__.  
Personally, I tested this setup by using a key in a JSON file and setting the `GOOGLE_APPLICATION_CREDENTIALS`
environment variable in `build.gradle.kts`.
[The documentation can be found here.](https://cloud.google.com/docs/authentication/provide-credentials-adc#local-key)  
This may not be the approach you want to follow, so definitely read [the documentation](https://cloud.google.com/docs/authentication/provide-credentials-adc),
compare the alternatives and choose wisely :)

## Sources

In addition to the ones linked above, [this article](https://medium.com/net-core/using-google-cloud-storage-in-asp-net-core-74f9c5ee55f5)
can show you how to create the bucket and set the right permissions from within the GCloud dashboard.
