plugins {
    java
    id("org.springframework.boot") version "3.2.5"
    id("io.spring.dependency-management") version "1.1.4"
}

group = "be.kdg.integration4"
version = "0.0.1-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

repositories {
    mavenCentral()
}

extra["springCloudGcpVersion"] = "5.1.2"
extra["springCloudVersion"] = "2023.0.1"

val bootRun by tasks.getting(JavaExec::class) {
    environment("GOOGLE_APPLICATION_CREDENTIALS", "You don't have my key file, so you're out of luck xD --- this should be a file path btw")
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.google.cloud:spring-cloud-gcp-starter-storage")
    developmentOnly("org.springframework.boot:spring-boot-devtools")
    runtimeOnly("org.webjars:bootstrap:5.3.3")
}

dependencyManagement {
    imports {
        mavenBom("com.google.cloud:spring-cloud-gcp-dependencies:${property("springCloudGcpVersion")}")
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
    }
}
